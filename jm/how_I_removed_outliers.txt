method to find outliers to delete
deleted all loss ratio greater than 1000
deleted all premiums less than 60
414417 records remaining

Nare's features and targets in first iteration of regression:
features = ['Driver_Total', 'Driver_Total_Male', 'Driver_Total_Female', 'Driver_Total_Single', 'Driver_Total_Married','Driver_Minimum_Age', 'Driver_Maximum_Age', 'Driver_Total_Teenager_Age_15_19', 'Driver_Total_College_Ages_20_23', 'Driver_Total_Young_Adult_Ages_24_29', 'Driver_Total_Low_Middle_Adult_Ages_30_39', 'Driver_Total_Middle_Adult_Ages_40_49', 'Driver_Total_Adult_Ages_50_64', 'Driver_Total_Senior_Ages_65_69', 'Driver_Total_Upper_Senior_Ages_70_plus']
    target = ['Annual_Premium', 'Loss_Amount', 'Loss_Ratio']


Tieming used infogain to find most useful attributes to try in second iteration
vehicle_make_year
vehicle_symbol
vehicle_age
vehicle_collision_coverage
vehicle_collission_coverage_deductible
driver_minimum_age
driver_maximum_age
EEA_liability_coverage_only_indicator
EEA_full_coverage_indicator
Sys_Renewed