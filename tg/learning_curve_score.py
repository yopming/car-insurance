import pandas
import numpy as np
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeRegressor
from sklearn.datasets import load_digits
from sklearn.model_selection import learning_curve
from sklearn.model_selection import ShuffleSplit

infogain_list = [
    'SYS_Renewed', ' Vehicle_Collision_Coverage_Deductible',
    ' EEA_Full_Coverage_Indicator',
    ' Vehicle_Collision_Coverage_Indicator', ' EEA_Liability_Coverage_Only_Indicator',
    ' Vehicle_Age_In_Years', ' Vehicle_Make_Year', ' Driver_Minimum_Age',
    ' Vehicle_Symbol', ' Driver_Maximum_Age', ' Vehicle_Anti_Theft_Device',
    ' Vehicle_Passive_Restraint', ' Vehicle_Miles_To_Work',
    ' EEA_Packaged_Policy_Indicator',
    ' Vehicle_New_Cost_Amount', ' Policy_Zip_Code_Garaging_Location',
    ' EEA_Policy_Zip_Code_3', ' Vehicle_Usage', ' EEA_Policy_Tenure',
    ' Policy_Installment_Term', ' SYS_New_Business',
    ' Vehicle_Comprehensive_Coverage_Limit',
    ' Vehicle_Bodily_Injury_Limit', ' EEA_Prior_Bodily_Injury_Limit',
    ' Vehicle_Physical_Damage_Limit', ' Driver_Total_Young_Adult_Ages_24_29',
    ' Policy_Billing_Code', ' Driver_Total_Female',
    ' Driver_Total_Upper_Senior_Ages_70_plus',
    ' Vehicle_Territory', ' Driver_Total_Male', ' Driver_Total_Married',
    ' Vehicle_Performance', ' Policy_Method_Of_Payment',
    ' Vehicle_Number_Of_Drivers_Assigned', ' Driver_Total_Adult_Ages_50_64',
    ' Vehicle_Med_Pay_Limit', ' Vehicle_Driver_Points',
    ' Driver_Total_Middle_Adult_Ages_40_49', ' Driver_Total_Related_To_Insured_Child',
    ' Policy_Company', ' Driver_Total_Teenager_Age_15_19', ' EEA_Agency_Type',
    ' Driver_Total_Related_To_Insured_Spouse', ' Policy_Reinstatement_Fee_Indicator',
    ' Driver_Total_Senior_Ages_65_69', ' Driver_Total_Low_Middle_Adult_Ages_30_39',
    ' EEA_Multi_Auto_Policies_Indicator', ' Vehicle_Youthful_Driver_Training_Code',
    ' Vehicle_Safe_Driver_Discount_Indicator', ' Driver_Total_Single',
    ' Vehicle_Comprehensive_Coverage_Indicator', ' Vehicle_Youthful_Driver_Indicator',
    ' Driver_Total_Related_To_Insured_Self', ' Vehicle_Days_Per_Week_Driven',
    ' Driver_Total_Licensed_In_State', ' Vehicle_Annual_Miles', ' Driver_Total',
    ' Driver_Total_College_Ages_20_23', ' Vehicle_Youthful_Good_Student_Code']


def plot_learning_curve(estimator, title, X, y, ylim=None, cv=None,
                        n_jobs=1, train_sizes=np.linspace(.1, 1.0, 5)):
    plt.figure()
    plt.title(title)
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel("Training examples")
    plt.ylabel("Score")
    train_sizes, train_scores, test_scores = learning_curve(
        estimator, X, y, cv='', n_jobs=n_jobs, train_sizes=train_sizes)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.1,
                     color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color="g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
             label="Training score")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
             label="Cross-validation score")

    plt.legend(loc="best")
    return plt

data_frame = pandas.read_csv("data/clean2-0-10.csv", low_memory=False)
data_value = data_frame.values
y = data_value[:,-1]
X = np.delete(data_value, -1, 1)

title = "Learning Curves - 10 Features - Without Outliers - With 0 Claim"
cv = ShuffleSplit(n_splits=100, test_size=0.2, random_state=0)

estimator = DecisionTreeRegressor()
plot_learning_curve(estimator, title, X, y, ylim=(-2.5, 2.5), cv=cv, n_jobs=8)

plt.show()
