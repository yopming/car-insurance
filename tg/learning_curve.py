import math
import pandas
from matplotlib import pyplot as plot
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error


class LearningCurve:
    def __init__(self, datafile, regressor):
        self.regressor = regressor
        self.datafile = datafile
        self.infogain = [
            'Annual_Premium', 'SYS_Renewed', 'Vehicle_Collision_Coverage_Deductible',
            'EEA_Full_Coverage_Indicator',
            'Vehicle_Collision_Coverage_Indicator', 'EEA_Liability_Coverage_Only_Indicator',
            'Vehicle_Age_In_Years', 'Vehicle_Make_Year', 'Driver_Minimum_Age',
            'Vehicle_Symbol', 'Driver_Maximum_Age', 'Vehicle_Anti_Theft_Device',
            'Vehicle_Passive_Restraint', 'Vehicle_Miles_To_Work',
            'EEA_Packaged_Policy_Indicator',
            'Vehicle_New_Cost_Amount', 'Policy_Zip_Code_Garaging_Location',
            'EEA_Policy_Zip_Code_3', 'Vehicle_Usage', 'EEA_Policy_Tenure',
            'Policy_Installment_Term', 'SYS_New_Business',
            'Vehicle_Comprehensive_Coverage_Limit',
            'Vehicle_Bodily_Injury_Limit', 'EEA_Prior_Bodily_Injury_Limit',
            'Vehicle_Physical_Damage_Limit', 'Driver_Total_Young_Adult_Ages_24_29',
            'Policy_Billing_Code', 'Driver_Total_Female',
            'Driver_Total_Upper_Senior_Ages_70_plus',
            'Vehicle_Territory', 'Driver_Total_Male', 'Driver_Total_Married',
            'Vehicle_Performance', 'Policy_Method_Of_Payment',
            'Vehicle_Number_Of_Drivers_Assigned', 'Driver_Total_Adult_Ages_50_64',
            'Vehicle_Med_Pay_Limit', 'Vehicle_Driver_Points',
            'Driver_Total_Middle_Adult_Ages_40_49', 'Driver_Total_Related_To_Insured_Child',
            'Policy_Company', 'Driver_Total_Teenager_Age_15_19', 'EEA_Agency_Type',
            'Driver_Total_Related_To_Insured_Spouse', 'Policy_Reinstatement_Fee_Indicator',
            'Driver_Total_Senior_Ages_65_69', 'Driver_Total_Low_Middle_Adult_Ages_30_39',
            'EEA_Multi_Auto_Policies_Indicator', 'Vehicle_Youthful_Driver_Training_Code',
            'Vehicle_Safe_Driver_Discount_Indicator', 'Driver_Total_Single',
            'Vehicle_Comprehensive_Coverage_Indicator', 'Vehicle_Youthful_Driver_Indicator',
            'Driver_Total_Related_To_Insured_Self', 'Vehicle_Days_Per_Week_Driven',
            'Driver_Total_Licensed_In_State', 'Vehicle_Annual_Miles', 'Driver_Total',
            'Driver_Total_College_Ages_20_23', 'Vehicle_Youthful_Good_Student_Code']

    def plot_on_datasize(self, start, step):
        rmse_keys = []
        rmse_list = []
        rmse_prime_list = []
        for i in range(start, 400001, step):
            rmse_keys.append(i)
            df = pandas.read_csv(self.datafile, nrows=i, low_memory=False)

            rmse, rmse_prime = self.rmse(self.infogain, df)
            rmse_list.append(rmse)
            rmse_prime_list.append(rmse_prime)

        return self.plotting(rmse_list, rmse_prime_list, rmse_keys, "Training Example")

    def plot_on_feature(self, start, step, features=[]):
        if features:
            df = pandas.read_csv(self.datafile, low_memory=False, names=features)
            feature_list = features
        else:
            df = pandas.read_csv(self.datafile, low_memory=False)
            feature_list = self.infogain

        col_number = df.shape[1]

        rmse_keys = []
        rmse_train_list = []
        rmse_pred_list = []

        for i in range(start, col_number, step):
            _df = df
            feature_list = feature_list[0:i]
            rmse_train, rmse_pred = self.rmse(feature_list, _df)

            rmse_keys.append(i)
            rmse_train_list.append(rmse_train)
            rmse_pred_list.append(rmse_pred)

        return self.plotting(rmse_train_list, rmse_pred_list, rmse_keys, "Feature Size")

    def rmse(self, feature_list, df):
        train_df, test_df = train_test_split(df, test_size=0.2)
        target_name = ['Loss_Amount']
        train_data_x = train_df[feature_list].values
        train_data_y = train_df[target_name].values
        test_data_x = test_df[feature_list].values
        test_data_y = test_df[target_name].values

        clf = self.regressor
        clf = clf.fit(train_data_x, train_data_y)
        train_y_pred = clf.predict(train_data_x)
        test_y_pred = clf.predict(test_data_x)

        mse_train = mean_squared_error(train_data_y, train_y_pred)
        rmse_train = math.sqrt(mse_train)

        mse_pred = mean_squared_error(test_data_y, test_y_pred)
        rmse_pred = math.sqrt(mse_pred)

        return rmse_train, rmse_pred

    @staticmethod
    def plotting(line1, line2, keys, xlabel=None, title=None):
        plot.figure()
        plot.title(title)
        plot.xlabel(xlabel)
        plot.ylabel("RMSE")
        plot.plot(keys, line1, 'o-', color="r", label="Training RMSE")
        plot.plot(keys, line2, 'o-', color="b", label="Prediction RMSE")
        plot.legend(loc="best")
        return plot


if __name__ == '__main__':
    from sklearn import tree
    curve = LearningCurve("data/clean2-0.csv", tree.DecisionTreeRegressor(max_depth=4))
    plotting = curve.plot_on_datasize(50000, 50000)
    # plotting = curve.plot_on_feature(10, 5)
    plotting.show()
