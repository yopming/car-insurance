import csv
from math import log

datasource = "clean1-2.csv"
result_datasource = "clean1-2.csv"


def read_attr_name():
    with open(datasource, newline='') as f:
        reader = csv.reader(f)
        attrs = next(reader)
        return attrs


def read_result():
    with open(result_datasource, 'r') as f:
        reader = csv.DictReader(f)
        data = {}
        for row in reader:
            for header, value in row.items():
                try:
                    data[header].append(value)
                except KeyError:
                    data[header] = [value]
    return data['Loss_Ratio_Group']


# Get data from csv file
def read_attrs():
    with open(datasource, 'r') as f:
        reader = csv.DictReader(f)
        data = {}
        for row in reader:
            for header, value in row.items():
                try:
                    data[header].append(value)
                except KeyError:
                    data[header] = [value]
    return data


def entropy(seq):
    sequence = set(seq)
    prob = lambda x: sum(1.0 for i in seq if i == x) / len(seq)
    result = - sum(prob(i) * log(prob(i), 2) for i in sequence)
    return result


# Information Gain function
def info_gain(attr_data, samp_data):
    samp_entropy = entropy(samp_data)
    attr_value = list(set(attr_data))
    attr_v = 0
    for i in range(len(attr_value)):
        index_list = list()
        for j in range(len(attr_data)):
            _data = attr_data[j]
            _value = attr_value[i]
            if _value == _data:
                index_list.append(j)
        prob = len(index_list) / float(len(samp_data))
        samp_value = [samp_data[k] for k in index_list]
    attr_v += prob * entropy(samp_value)
    result = samp_entropy - attr_v
    return result


if __name__ == '__main__':
    attr_names = read_attr_name()
    loss_ratio = read_result()
    data = read_attrs()

    for name in attr_names:
        if name not in ['PolicyNo', 'Vehicle_Make_Description']:
            ig = info_gain(data[name], loss_ratio)
            print(name, "info_gain: ", ig)
